import Home from '@/components/Home';
import About from '@/components/About';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        component: About
    }
];

export default routes;
